import React, { Component, useEffect, useState } from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Bienvenido </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 0, 71, 0.5)',
    alignItems: 'center',
    justifyContent:"center"
  },
  title: {
    color: 'white',
    fontSize:50,
  },
});
