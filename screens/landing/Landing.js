import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import { Button } from 'react-native-elements';
const image = {
  uri: 'https://picsum.photos/200/300',
};

export default class LandingScreen extends Component {

  render() {
    return (
      <ImageBackground source={image} style={styles.image}>
        <View style={styles.overlay}>
          <View style={styles.logo}>
            <Text style={styles.title}>Bienvenido a Save Money</Text>
          </View>

          <View style={styles.btn}>
            <Button
              buttonStyle={{
                backgroundColor: 'white',
                width: 200,
              }}
              titleStyle={{
                color: '#f75b5b',
              }}
              onPress={() => this.props.navigation.navigate('Login')}
              title="Login"
            />
            <Button
              buttonStyle={{ color: 'white', width: 200, borderColor: 'white' }}
              titleStyle={{
                color: 'white',
              }}
              title="Registrate"
              type="outline"
            />
          </View>
          <View style={styles.footer}>
            <Text style={styles.footerText}>Terminos y condiciones</Text>
            <Text style={styles.footerText}>Privacidad</Text>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  btn: {
    flexDirection: 'column',
    height: 150,
    justifyContent: 'space-evenly',
    alignSelf: 'center',
    bottom: -50,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    bottom: -50,
  },
  footerText: {
    color: 'white',
  },
  overlay: {
    flex: 1,
    height: '100%',
    width: '100%',
    position: 'absolute',
    backgroundColor: 'rgba(255, 0, 71, 0.5)',
    justifyContent: 'space-evenly',
    flexDirection: 'column',
  },
});
