import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button } from 'react-native';
import {SelectList, MultipleSelectList }from 'react-native-dropdown-select-list'

export default function App() {

  const [selected, setSelected] = React.useState("");


  const data = [
    {key:'Canada', value:'Canada'},
    {key:'England', value:'England'},
    {key:'Pakistan', value:'Pakistan'},
    {key:'India', value:'India'},
    {key:'NewZealand', value:'NewZealand'},
  ]

  const sinTecho = [
    {id: 1, nombre: 'st-1'},
    {id: 2, nombre: 'st-2'},
    {id: 3, nombre: 'st-3'},
    {id: 4, nombre: 'st-4'},
    {id: 5, nombre: 'st-5'},
    {id: 6, nombre: 'st-6'},
    {id: 7, nombre: 'st-7'},
  ]

  const conTecho = [
    {id: 1, nombre: 'ct-1'},
    {id: 2, nombre: 'ct-2'},
    {id: 3, nombre: 'ct-3'},
    {id: 4, nombre: 'ct-4'},
    {id: 5, nombre: 'ct-5'},
    {id: 6, nombre: 'ct-6'},
    {id: 7, nombre: 'ct-7'},
  ]

  const listaBotones = sinTecho.map((number) =>
    <TouchableOpacity key={number.id} style={{ backgroundColor: "red", padding: 15, margin:5  }}><Text>{number.nombre}</Text></TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <View>
        <View>
          <Text>Tipo de Reserva: </Text>
          <SelectList setSelected={setSelected} data={data}  />
        </View>

        <View>
          <Text>Fecha de Inicio: </Text>
          <SelectList setSelected={setSelected} data={data}  />
        </View>
      </View>
      
      <View>
        <View>
          <Text>Nivel: </Text>
          <SelectList setSelected={setSelected} data={data}  />
        </View>

        <View>
          <Text>Fecha Fin: </Text>
          <SelectList setSelected={setSelected} data={data}  />
        </View>
      </View>

      <View>
        <Text>Resultados</Text>
      </View>

      <View>
        <View style={{ backgroundColor: "blue" }}>
          <Text>Estacionamiento bajo techo</Text>
          <View>
            {listaBotones}
            {/* {[...Array(sinTecho).keys()].map(key => {
            return (
              <View key={key}>
                <TouchableOpacity><Text>Boton -  </Text> </TouchableOpacity>
              </View>
                // <View key={key} {...slider.slidesProps[key]}>
                //   <View style={{...styles.slide, backgroundColor: colors[key]}}>
                //     <Text style={styles.text}>Slide {key + 1}</Text>
                //   </View>
                // </View>
              )
            })} */}
          </View>
        </View>
        <View style={{ backgroundColor: "green" }}>
          <Text>Estacionamiento sin techo</Text>
          <View></View>
        </View>
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
